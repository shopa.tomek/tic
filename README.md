## tictak
```
Simple tic tac toe game
```
## dependencies:
```
Vuejs3
```
## Project setup
```
clone project and then run npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```
### Modify package.json file (like below) if there's a problem with app to start (npm run serve doesn't work). It may happend on Windows.
```
"scripts": {
    "serve": "vue-cli-service.cmd serve",
    "build": "vue-cli-service.cmd build",
    "lint": "vue-cli-service.cmd lint"
  },

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
